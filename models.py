import webapp2
import logging

from google.appengine.ext import db
from google.appengine.api import memcache

class Player(db.Model):
    name = db.StringProperty()
    created = db.DateTimeProperty(auto_now_add=True)

    def __memcache_num_won_key(self):
        return "player_" + str(self.key().id()) + "_num_won"

    def __memcache_games_key(self):
        return "player_" + str(self.key().id()) + "_games"

    def get_games(self):
        memcache_games = memcache.get(self.__memcache_games_key())

        if memcache_games is not None:
            return memcache_games
        else:
            games = list(self.game_team1_attack_set) + \
                    list(self.game_team1_defense_set) + \
                    list(self.game_team2_attack_set) + \
                    list(self.game_team2_defense_set)

            memcache.set(self.__memcache_games_key(), games)
            return games

    def get_num_played(self):
        return len(self.get_games())

    def invalidate_memcache(self):
        memcache.delete(self.__memcache_num_won_key())
        memcache.delete(self.__memcache_games_key())

    def get_num_won(self):
        memcache_num_won = memcache.get(self.__memcache_num_won_key())

        if memcache_num_won is not None:
            return memcache_num_won
        else:
            won_games = filter(lambda g: g.in_winning_team(self), self.get_games())
            num_won = len(won_games)
            memcache.set(self.__memcache_num_won_key(), num_won)
            return num_won

    def get_num_lost(self):
        return self.get_num_played() - self.get_num_won()
        
    def get_quotient(self):
        if self.get_num_played() < 1:
            return 0
        return round(float(self.get_num_won()) / float(self.get_num_played()), 3 )



class Game(db.Model):
    played = db.DateTimeProperty(auto_now_add=True)

    team1_attack_count_goals = db.IntegerProperty()
    team1_attack_count_own_goals = db.IntegerProperty()
    team1_attack_player = db.ReferenceProperty(Player, None,
            "game_team1_attack_set")

    team1_defense_count_goals = db.IntegerProperty()
    team1_defense_count_own_goals = db.IntegerProperty()
    team1_defense_player = db.ReferenceProperty(Player, None,
            "game_team1_defense_set")
    
    team2_attack_count_goals = db.IntegerProperty()
    team2_attack_count_own_goals = db.IntegerProperty()
    team2_attack_player = db.ReferenceProperty(Player, None,
            "game_team2_attack_set")

    team2_defense_count_goals = db.IntegerProperty()
    team2_defense_count_own_goals = db.IntegerProperty()
    team2_defense_player = db.ReferenceProperty(Player, None,
            "game_team2_defense_set")

    submitter = db.UserProperty(auto_current_user_add=True)

    def get_team1_players(self):
        return [self.team1_attack_player, self.team1_defense_player]

    def get_team2_players(self):
        return [self.team2_attack_player, self.team2_defense_player]

    def get_team1_score(self):
        return self.team1_attack_count_goals \
                + self.team1_defense_count_goals \
                + self.team2_attack_count_own_goals \
                + self.team2_defense_count_own_goals

    def get_team2_score(self):
        return self.team2_attack_count_goals \
                + self.team2_defense_count_goals \
                + self.team1_attack_count_own_goals \
                + self.team1_defense_count_own_goals


    def in_winning_team(self, player):
        if self.get_team1_score() > self.get_team2_score():
            winning_team_players = self.get_team1_players()
        else:
            winning_team_players = self.get_team2_players()

        return player.key().id() in [p.key().id() for p in winning_team_players]
