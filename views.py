import webapp2
import jinja2
import logging
import os
import cgi

from models import *

jinja_environment = jinja2.Environment(
        loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class MainPage(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('/templates/index.html')
        self.response.out.write(template.render())

class AddPlayerPage(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template('/templates/add_player.html')
        self.response.out.write(template.render())
    def post(self):
        player_name = self.request.get('name').strip()

        error = None

        if player_name == '':
            error = "Kein Spielername angegeben."
        elif len(list(Player.all().filter('name = ', player_name))) > 0:
            error = "Spieler mit diesem Namen existiert bereits."

        if error is None:
            player = Player(name=self.request.get('name'))
            player.put()
            template_values = {'added_player': player}
        else:
            template_values = {'error': error}

        template = jinja_environment.get_template('/templates/add_player.html')
        self.response.out.write(template.render(template_values))

class AddGamePage(webapp2.RequestHandler):
    def get(self):
        players = Player.all().order("name")

        template = jinja_environment.get_template('/templates/add_game.html')
        self.response.out.write(template.render({
            'players': players
        }))

    def post(self):
        # TODO: sicherstellen dass es 4 verschiedene spieler sind
        # TODO: fehlerbehandlung, falls nicht alle information da ist
        # TODO: sanity check: beide teams gleichen score verhindern

        player_team1_attack = Player.get_by_id(int(self.request.get('player_team1_attack_id')))
        player_team1_defense = Player.get_by_id(int(self.request.get('player_team1_defense_id')))
        player_team2_attack = Player.get_by_id(int(self.request.get('player_team2_attack_id')))
        player_team2_defense = Player.get_by_id(int(self.request.get('player_team2_defense_id')))
        
        game = Game()

        game.team1_attack_player = player_team1_attack
        game.team1_attack_count_goals = int(self.request.get('goals_team1_attack'))
        game.team1_attack_count_own_goals = int(self.request.get('own_goals_team1_attack'))

        game.team1_defense_player = player_team1_defense
        game.team1_defense_count_goals = int(self.request.get('goals_team1_defense'))
        game.team1_defense_count_own_goals = int(self.request.get('own_goals_team1_defense'))

        game.team2_attack_player = player_team2_attack
        game.team2_attack_count_goals = int(self.request.get('goals_team2_attack'))
        game.team2_attack_count_own_goals = int(self.request.get('own_goals_team2_attack'))

        game.team2_defense_player = player_team2_defense
        game.team2_defense_count_goals = int(self.request.get('goals_team2_defense'))
        game.team2_defense_count_own_goals = int(self.request.get('own_goals_team2_defense'))

        game.put()

        player_team1_attack.invalidate_memcache()
        player_team1_defense.invalidate_memcache()
        player_team2_attack.invalidate_memcache()
        player_team2_defense.invalidate_memcache()

class ResultsPage(webapp2.RequestHandler):
    def calc_goals_shot(self, games):
        goals_shot = 0
        for game in games:
            goals_shot += game.get_team1_score() + game.get_team2_score();
        return goals_shot 



    def get(self):
        template = jinja_environment.get_template('/templates/results.html')
        games = Game.all().order('-played')
        
        players_by_won_games = list(Player.all())
        players_by_won_games.sort(lambda a, b: cmp(b.get_num_won(), a.get_num_won()))

        goals_shot = self.calc_goals_shot(games)
        costs_euro = round(goals_shot / 9 * 0.5, 2)

        template_values = {
                'games': games,
                'players_by_won_games': players_by_won_games,
                'goals_shot': self.calc_goals_shot(games),
                'costs_euro': costs_euro
        }

        self.response.out.write(template.render(template_values))
