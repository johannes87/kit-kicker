import webapp2

from models import *
from views import *

app = webapp2.WSGIApplication([('/', MainPage),
                               ('/add_player', AddPlayerPage),
                               ('/add_game', AddGamePage),
                               ('/results', ResultsPage)],
                              debug=True)

